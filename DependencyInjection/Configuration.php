<?php
namespace Innomedio\TranslationsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('innomedio_translations');

        $rootNode
            ->children()
                ->booleanNode('translate_backend')
                    ->defaultValue(false)
                ->end()
            ->end();

        return $treeBuilder;
    }
}
