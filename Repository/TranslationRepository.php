<?php

namespace Innomedio\TranslationsBundle\Repository;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityRepository;

class TranslationRepository extends EntityRepository
{
    public function getAllTranslations($includeBackend = false)
    {
        $qb = $this
            ->createQueryBuilder('t')
            ->orderBy('t.translationKey');

        if ($includeBackend === false) {
            $qb
                ->where('t.translationKey NOT LIKE :parameter')
                ->setParameter('parameter', 'app.backend_translation%');
        }

        return $qb
            ->getQuery()
            ->execute();
    }

    /**
     * @return bool
     * @throws DBALException
     */
    public function setAllTranslationsAsExported()
    {
        $sql = "
            UPDATE translation
            SET exported = 1
        ";

        $statement = $this->getEntityManager()->getConnection()->prepare($sql);
        $statement->execute();

        return true;
    }

    /**
     * @param $search
     * @return mixed
     */
    public function findLike($search)
    {
        $qb = $this
            ->createQueryBuilder('t')
            ->leftJoin('t.translations', 'tt')
            ->where('tt.value LIKE :search')
            ->orWhere('t.translationKey LIKE :search')
            ->setParameter('search', '%' . $search . '%')
            ->getQuery();

        return $qb->execute();
    }

    /**
     * @throws DBALException
     */
    public function hasMissingTranslations()
    {
        $sql = "
            SELECT
                a.*,
                b.*,
                b.id AS translationId,
                c.value
            FROM language AS a
    
            LEFT JOIN translation AS b ON b.id  > 0
    
            LEFT JOIN translation_translation AS c
            ON c.language_id = a.id AND c.translation_id = b.id
    
            WHERE value IS NULL OR value = ''
    
            ORDER BY a.id ASC
        ";

        $statement = $this->getEntityManager()->getConnection()->prepare($sql);
        $statement->execute();

        return $statement->rowCount();
    }
}
