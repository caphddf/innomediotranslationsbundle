# Innomedio Translations Bundle

* Creates a table used for managing all translation keys
* Adds an interface to the InnomedioBackendThemeBundle for managing all translations
* Makes it possible to export translations to the website and clears the cache
* Automatically adds translations missing translations from your project to the database
* ROLE_ROOT users can add translations

## Documentation

* [Installation](Resources/docs/installation.md)
* [Configuration](Resources/docs/configuration.md) (TODO)