<?php
namespace Innomedio\TranslationsBundle\Controller\Backend;

use Doctrine\DBAL\DBALException;
use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\BackendThemeBundle\Service\Ajax\ErrorParser;
use Innomedio\TranslationsBundle\Entity\Translation;
use Innomedio\TranslationsBundle\Entity\TranslationTranslation;
use Innomedio\TranslationsBundle\Form\TranslationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TranslationController
 * @Security("has_role('ROLE_TRANSLATIONS')")
 * @Route("/admin/translations")
 */
class TranslationController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.translations.index")
     * @return Response
     */
    public function list()
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.translations.translations');

        return $this->render('@InnomedioTranslations/list.html.twig', array(
            'languages' => $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll(),
            'newTranslations' => $this->getDoctrine()->getRepository('InnomedioTranslationsBundle:Translation')->findBy(array('exported' => false)),
            'allTranslations' => $this->getDoctrine()->getRepository('InnomedioTranslationsBundle:Translation')->getAllTranslations($this->isGranted('ROLE_ROOT'))
        ));
    }

    /**
     * @Route(
     *     "/{action}/{id}",
     *     name="innomedio.translations.form",
     *     requirements={
     *         "action" = "add|edit",
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @ParamConverter("translation", class="Innomedio\TranslationsBundle\Entity\Translation")
     *
     * @param $action
     * @param Translation|null $translation
     *
     * @return Response
     */
    public function form($action, Translation $translation = null)
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.translations.translations', $this->generateUrl('innomedio.translations.index'));
        $this->header()->addBreadcrumb('innomedio.translations.header.title_' . $action);

        $form = $this->createForm(TranslationType::class, $translation);
        $form->add('submit', SubmitType::class, array(
            'label' => 'innomedio.backend_theme.global.' . $action
        ));

        return $this->render('@InnomedioTranslations/form.html.twig', array(
            'languages' => $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll(),
            'translation' => $translation,
            'form' => $form->createView(),
            'activeNav' => 'admin',
            'activeSubnav' => 'translations'
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.translations.submit"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function submit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $response = new AjaxResponse();

        if ($id > 0) {
            $translation = $this->getDoctrine()->getRepository('InnomedioTranslationsBundle:Translation')->find($id);
            $action = 'edit';
        } else {
            $translation = new Translation();
            $action = 'add';
        }

        $form = $this->createForm(TranslationType::class, $translation);
        $form->handleRequest($request);

        $translation->setExported(false);

        if ($form->isSubmitted() && $form->isValid()) {
            $translationFields = $request->request->get('translation')['translation'];

            foreach ($translationFields as $languageId => $data) {
                if ($translation->hasTranslation($languageId)) {
                    $object = $translation->getTranslations()->get($languageId);
                    $object->setValue($data['translationValue']);

                    $em->persist($object);
                } else {
                    $language = $em->getRepository('InnomedioBackendThemeBundle:Language')->find($languageId);

                    $object = new TranslationTranslation();
                    $object->setValue($data['translationValue']);
                    $object->setLanguage($language);

                    $translation->addTranslation($object);
                }
            }

            $em->persist($translation);
            $em->flush();

            $response->setSuccess(true);
            $response->setRedirect($this->generateUrl('innomedio.translations.index'));
            $response->setMessage($this->get('translator')->trans('innomedio.translations.submit.success.' . $action));
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $translation));
        }

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.translations.remove")
     * @ParamConverter("translation", class="Innomedio\TranslationsBundle\Entity\Translation")

     * @param Translation $translation
     *
     * @return JsonResponse
     */
    public function ajaxRemove(Translation $translation)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($translation);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }

    /**
     * @Route("/export-for-website", name="innomedio.translations.ajax_export_for_website")
     *
     * @return JsonResponse
     * @throws DBALException
     */
    public function ajaxExportForWebsite()
    {
        $fs = $this->get('filesystem');
        $trans = $this->get('translator');

        $translations = $this->getDoctrine()->getRepository('InnomedioTranslationsBundle:Translation')->findAll();

        $array = array();

        foreach ($translations as $translation) {
            foreach ($translation->getTranslations() as $localeTranslation) {
                $array[$localeTranslation->getLanguage()->getCode()][$translation->getTranslationKey()] = $localeTranslation->getValue();
            }
        }

        foreach ($array as $languageCode => $data) {
            $content = $this->renderView('@InnomedioTranslations/xliff_website_template.xlf.twig', array(
                'keys' => $data,
                'language' => $languageCode
            ));

            $fs->dumpFile($this->getParameter('kernel.project_dir') . '/translations/messages.' . $languageCode . '.xlf', $content);
        }

        $fs->remove(array($this->getParameter('kernel.cache_dir') . '/translations/'));
        $this->getDoctrine()->getRepository('InnomedioTranslationsBundle:Translation')->setAllTranslationsAsExported();

        return new JsonResponse(array(
            'succes' => true,
            'message' => $trans->trans('innomedio.translations.export_success_message'),
            'title' => $trans->trans('innomedio.translations.export_success_title')
        ));
    }

    /**
     * @Route("/ajax-search", name="innomedio.translations.ajax_search")
     *
     * @param $request Request
     *
     * @return Response
     */
    public function ajaxSearch(Request $request)
    {
        $search = $request->request->get('search');

        if ($request->request->get('showMissing')) {
            // @todo
        } else {
            $translations = $this->getDoctrine()->getRepository('InnomedioTranslationsBundle:Translation')->findLike($search);
        }

        $languages = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll();

        return $this->render('@InnomedioTranslations/search.html.twig', array(
            'languages' => $languages,
            'translations' => $translations
        ));
    }
}