# 1.0.1

* Changed route file for BackendThemeBundle 1.1 change (subdomain) and the documentation for implementation.
* Removed the unnecessary setup command
* There's a config value available now to prevent translating keys in the backend
* New guideline for adding backend translations

# 1.0

* Start!