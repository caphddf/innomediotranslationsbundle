<?php
namespace Innomedio\TranslationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Innomedio\BackendThemeBundle\Entity\Language;

/**
 * TranslationTranslation
 *
 * @ORM\Table(name="translation_translation", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="translation_language_translation", columns={"language_id", "translation_id"})
 * })
 * @ORM\Entity(repositoryClass="Innomedio\TranslationsBundle\Repository\TranslationTranslationRepository")
 */
class TranslationTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\BackendThemeBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $language;

    /**
     * @var Translation
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\TranslationsBundle\Entity\Translation", inversedBy="translations")
     * @ORM\JoinColumn(name="translation_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $translation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return Translation
     */
    public function getTranslation(): Translation
    {
        return $this->translation;
    }

    /**
     * @param Translation $translation
     */
    public function setTranslation(Translation $translation): void
    {
        $this->translation = $translation;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    /**
     * @param Language $language
     */
    public function setLanguage(Language $language): void
    {
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}