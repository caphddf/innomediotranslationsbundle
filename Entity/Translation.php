<?php
/**
 * @todo loggable maken
 */

namespace Innomedio\TranslationsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Innomedio\TranslationsBundle\Entity\TranslationTranslation;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Translation
 *
 * @ORM\Table(name="translation")
 * @ORM\Entity(repositoryClass="Innomedio\TranslationsBundle\Repository\TranslationRepository")
 * @UniqueEntity(
 *     fields={"translationKey"},
 *     message="innomedio.translations.duplicate_key"
 * )
 */
class Translation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="exported", type="boolean")
     */
    private $exported = false;

    /**
     * @ORM\Column(type="string", length=120, unique=true)
     */
    private $translationKey;

    /**
     * @var ArrayCollection|TranslationTranslation[]
     *
     * @ORM\OneToMany(targetEntity="Innomedio\TranslationsBundle\Entity\TranslationTranslation", mappedBy="translation", cascade={"persist", "remove"}, indexBy="language_id")
     */
    private $translations;

    /**
     * Translation constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTranslationKey()
    {
        return $this->translationKey;
    }

    /**
     * @param mixed $translationKey
     */
    public function setTranslationKey($translationKey)
    {
        $this->translationKey = $translationKey;
    }

    /**
     * @return bool
     */
    public function isExported(): bool
    {
        return $this->exported;
    }

    /**
     * @param bool $exported
     */
    public function setExported(bool $exported): void
    {
        $this->exported = $exported;
    }

    /**
     * @return ArrayCollection|TranslationTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param TranslationTranslation $translation
     */
    public function addTranslation(TranslationTranslation $translation)
    {
        if ($this->translations->contains($translation)) {
            return;
        }

        $this->translations[] = $translation;
        $translation->setTranslation($this);
    }

    /**
     * @param TranslationTranslation $translation
     * @return bool
     */
    public function hasTranslation($languageId)
    {
        return $this->getTranslations()->containsKey($languageId);
    }
}

