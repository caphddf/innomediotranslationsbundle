<?php
namespace Innomedio\TranslationsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Service\Message\Message;
use Innomedio\BackendThemeBundle\Service\Message\MessageExtension;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

class MissingTranslationsMessage extends MessageExtension
{
    private $translator;
    private $em;
    private $router;

    /**
     * MissingTranslationsMessage constructor.
     * @param EntityManagerInterface $em
     * @param TranslatorInterface $translator
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator, RouterInterface $router)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->router = $router;
    }

    /**
     * @return array|Message[]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getMessages()
    {
        return array(
            $this->missingTranslations(),
            $this->exportBehind()
        );
    }

    /**
     * @return Message
     * @throws \Doctrine\DBAL\DBALException
     */
    private function missingTranslations()
    {
        $check = $this->em->getRepository('InnomedioTranslationsBundle:Translation')->hasMissingTranslations();

        if ($check) {
            $message = new Message();
            $message->setText($this->translator->trans('innomedio.translations.dashboard.missing'));
            $message->setLink($this->router->generate('innomedio.translations.index'));
            $message->setRole('ROLE_TRANSLATIONS');
            $message->setType('warning');
            $message->setIcon('fa fa-tags');

            return $message;
        }

        return null;
    }

    /**
     * @return bool|Message
     */
    private function exportBehind()
    {
        $check = $this->em->getRepository('InnomedioTranslationsBundle:Translation')->findBy(array('exported' => false), array(), 1);

        if ($check) {
            $message = new Message();
            $message->setText($this->translator->trans('innomedio.translations.dashboard.export_warning'));
            $message->setLink($this->router->generate('innomedio.translations.index'));
            $message->setRole('ROLE_TRANSLATIONS');
            $message->setType('warning');
            $message->setIcon('fa fa-tags');

            return $message;
        }

        return false;
    }
}