<?php
namespace Innomedio\TranslationsBundle\Service;

use Innomedio\BackendThemeBundle\Service\Helper\BackendLocationHelper;
use Innomedio\TranslationsBundle\Entity\Translation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\MessageCatalogueInterface;
use Symfony\Component\Translation\TranslatorBagInterface;
use Symfony\Component\Translation\TranslatorInterface;

class TranslatorDecorator implements TranslatorInterface, TranslatorBagInterface
{
    /** @var TranslatorBagInterface|TranslatorInterface */
    private $translator;

    private $doctrine;
    private $helper;
    private $translateBackend;

    private $keysChecked = array();

    /**
     * TranslatorDecorator constructor.
     * @param TranslatorInterface $translator
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(TranslatorInterface $translator, EntityManagerInterface $doctrine, BackendLocationHelper $helper, $translateBackend)
    {
        $this->doctrine = $doctrine;
        $this->helper = $helper;
        $this->translateBackend = $translateBackend;

        return $this->translator = $translator;
    }

    /**
     * @param string $id
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return string
     */
    public function trans($id, array $parameters = array(), $domain = null, $locale = null)
    {
        $translation = $this->translator->trans($id, $parameters, $domain, $locale);

        /**
         * If the value == the key, it means that not translation is available, so we'll add it.
         * If it starts with "innomedio." though, the translation is from a bundle, which we don't want to add
         * here.
         */
        if (
            $translation == $id
            && !isset($this->keysChecked[$id])
            && substr($id, 0, 10) !== 'innomedio.'
            && (!$this->helper->isBackend() || ($this->helper->isBackend() && $this->translateBackend))
        ) {
            $check = $this->doctrine->getRepository('InnomedioTranslationsBundle:Translation')->findBy(array('translationKey' => $id));

            if (!$check) {
                $translationObj = new Translation();
                $translationObj->setTranslationKey($id);

                $this->doctrine->persist($translationObj);
                $this->doctrine->flush();
            }

            $this->keysChecked[$id] = true;
        }

        return $translation;
    }

    /**
     * @param string $id
     * @param int $number
     * @param array $parameters
     * @param null $domain
     * @param null $locale
     * @return string
     */
    public function transChoice($id, $number, array $parameters = array(), $domain = null, $locale = null)
    {
        return $this->translator->transChoice($id, $number, $parameters, $domain, $locale);
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        return $this->translator->setLocale($locale);
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->translator->getLocale();
    }

    /**
     * @param null $locale
     * @return MessageCatalogueInterface
     */
    public function getCatalogue($locale = null)
    {
        return $this->translator->getCatalogue($locale);
    }
}