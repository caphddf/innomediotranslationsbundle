<?php
namespace Innomedio\TranslationsBundle\Service;

use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarExtension;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarItem;
use Symfony\Component\Routing\Router;

class TranslationsSidebarExtension extends SidebarExtension
{
    private $sidebarContainer;
    private $router;

    /**
     * SidebarItems constructor.
     * @param SidebarContainer $sidebarContainer
     * @param Router $router
     */
    public function __construct(SidebarContainer $sidebarContainer, Router $router)
    {
        $this->sidebarContainer = $sidebarContainer;
        $this->router = $router;
    }

    /**
     * @return array|SidebarItem
     */
    public function getSidebars()
    {
        return array(
            $this->translations()
        );
    }

    /**
     * @return SidebarItem
     */
    public function translations()
    {
        $admin = $this->sidebarContainer->getMainItem('admin');

        $translations = new SidebarItem();
        $translations->setIcon('fa-tags');
        $translations->setName('innomedio.translations.translations');
        $translations->setLink($this->router->generate('innomedio.translations.index'));
        $translations->setTag('translations');
        $translations->setParent($admin);
        $translations->setRole('ROLE_TRANSLATIONS');

        return $translations;
    }
}