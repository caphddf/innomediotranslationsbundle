<?php

/* @todo https://symfony.com/doc/current/form/inherit_data_option.html */
/* @https://symfony.com/doc/current/form/create_custom_field_type.html */
/* https://gist.github.com/qrizly/2437078 */

namespace Innomedio\TranslationsBundle\Form;

use Innomedio\TranslationsBundle\Entity\Translation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translationKey', TextType::class, array(
                'label' => 'innomedio.translations.label.translation_key'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Translation::class,
            'allow_extra_fields' => true
        ));
    }
}