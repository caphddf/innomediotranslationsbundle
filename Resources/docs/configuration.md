# Configuration

In the frontend of your project, all unknown keys will automatically be added to the translations database. Translations
in you backend extensions won't be translated since in most cases you'll always be working in Dutch.

Still, if for any reason the backend needs to be multilingual too, you can active automatic adding of key like this:

```
innomedio_translations:
    translate_backend: false
```
    
**The prefix of any backend translation should always be *app.backend_translation*. This way the backend knows
that only root user can see and change these translations.**
